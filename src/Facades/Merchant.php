<?php

namespace GuanChanghu\Library\Facades;


use GuanChanghu\Library\Contracts\Merchant as MerchantContract;
use Illuminate\Support\Facades\Facade;

/**
 * @author 管昌虎
 * Class Merchant
 * @method static mixed get(string $key, mixed $value = null)
 * @package GuanChanghu\Library\Facades
 * Created on 2023/5/25 23:10
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Merchant extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return MerchantContract::class;
    }
}