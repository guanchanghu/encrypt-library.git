<?php
/**
 * 图片验证码核心契约
 * Created on 2022/4/22 15:12
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Captcha;


/**
 * @author 管昌虎
 * Class Core
 * @package GuanChanghu\Library\Contracts\Captcha
 * Created on 2022/4/22 15:12
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
interface Core
{
    /**
     * Core constructor.
     * @param Support $support
     */
    public function __construct(Support $support);

    /**
     * @param string $key
     * @return array
     */
    public function create(string $key = 'default'): array;

    /**
     * @param string $key
     * @param string $value
     * @return void
     */
    public function verify(string $key, string $value): void;

    /**
     * @param string $key
     * @param string $value
     * @return bool
     */
    public function checkout(string $key, string $value): bool;
}
