<?php
/**
 * 实人认证支持契约
 * Created on 2022/5/7 10:28
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Authentication;

/**
 * Class Support
 * @package GuanChanghu\Library\Contracts\Authentication
 * Created on 2022/5/7 10:28
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Support
{
    /**
     * Support constructor.
     * @param array $config
     */
    public function __construct(array $config = []);


    /**
     * @param string|int $key
     * @return array|string|int|float|null
     */
    public function config(string|int $key = ''): array|string|int|float|null;

    /**
     * @param string $orderNo
     * @param string $realName
     * @param string $idNumber
     * @param string $frontImage
     * @param string $backImage
     * @param string $handImage
     * @param array $params
     * @return array
     */
    public function token(string $orderNo, string $realName, string $idNumber, string $frontImage = '', string $backImage = '', string $handImage = '', array $params = []): array;

    /**
     * @param string $orderNo
     * @param string $realName
     * @param string $idNumber
     * @param string $frontImage
     * @param string $backImage
     * @param string $handImage
     * @param array $params
     * @return bool
     */
    public function verify(string $orderNo = '', string $realName = '', string $idNumber = '', string $frontImage = '', string $backImage = '', string $handImage = '', array $params = []): bool;
}
