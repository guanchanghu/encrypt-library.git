<?php


namespace GuanChanghu\Library\Contracts;

use Illuminate\Console\Command;

/**
 * Interface ProjectInstall
 * @package GuanChanghu\Library\Contracts
 */
interface ProjectInstall
{
    /**
     * @param Command $command
     * @return void
     */
    public function initialize(Command $command): void;

    /**
     * @param Command $command
     * @return void
     */
    public function createDatabase(Command $command): void;

    /**
     * @param Command $command
     * @return void
     */
    public function install(Command $command): void;

    /**
     * @return void
     */
    public function generateRsa(): void;

    /**
     * @param Command $command
     * @return void
     */
    public function updateRsa(Command $command): void;

    /**
     * @param Command $command
     * @return void
     */
    public function authorization(Command $command): void;
}
