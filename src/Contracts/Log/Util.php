<?php

namespace GuanChanghu\Library\Contracts\Log;


/**
 * @author 管昌虎
 * Interface Util
 * @package GuanChanghu\Library\Contracts\Log
 * Created on 2023/4/5 09:05
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
interface Util
{
    /**
     * @param string $message
     * @param array $context
     * @param string $level
     * @param string|null $channel
     * @param int $delay
     */
    public function __construct(string $message, array $context = [], string $level = 'info', string|null $channel = null, int $delay = 0);

    /**
     * @return void
     */
    public function handle();
}
