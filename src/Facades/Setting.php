<?php
/**
 * 环境设置
 * Created on 2022/8/26 9:25
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use Illuminate\Support\Facades\Facade;
use GuanChanghu\Library\Contracts\Setting as SettingContract;

/**
 * @author 管昌虎
 * Class Setting
 * @method static bool isInitialize()
 * @method static void initializeSetting()
 * @method static string getMacAddress()
 * @package GuanChanghu\Library\Facades
 * Created on 2022/8/26 9:26
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Setting extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return SettingContract::class;
    }
}
