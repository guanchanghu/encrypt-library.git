<?php


namespace GuanChanghu\Library\Contracts\Repositories;


use Illuminate\Contracts\Foundation\Application;

/**
 * Interface Factory
 * @package GuanChanghu\Library\Contracts\Repositories
 */
interface Factory
{
    /**
     * Factory constructor.
     * @param Application $app
     */
    public function __construct(Application $app);

    /**
     * 分页方式
     * @param string $way
     * @return Core
     */
    public function way(string $way = ''): Core;
}
