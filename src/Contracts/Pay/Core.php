<?php
/**
 * 支付核心
 * Created on 2022/3/17 14:59
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Pay;

use GuanChanghu\Library\Models\UserAccount;
use GuanChanghu\Library\Models\User;
use Closure;
use Psr\Http\Message\ResponseInterface;
use Yansongda\Pay\Exception\ContainerException;
use Yansongda\Pay\Exception\InvalidParamsException;
use Yansongda\Pay\Exception\ServiceNotFoundException;
use Yansongda\Supports\Collection;

/**
 * @author 管昌虎
 * Class Core
 * @package GuanChanghu\Library\Contracts\Pay
 * Created on 2022/3/17 15:00
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
interface Core
{
    /**
     * Core constructor.
     * @param App $app
     */
    public function __construct(App $app);

    /**
     * @param string $way
     * @param string $outTradeNo
     * @param float $amount
     * @param string $description
     * @param User|null $user
     * @param string $attach
     * @param array $other
     * @return ResponseInterface|Collection
     */
    public function pay(string $way, string $outTradeNo, float $amount, string $description, User|null $user = null, string $attach = '', array $other = array()): ResponseInterface|Collection;

    /**
     * @param Closure $callback
     * @return ResponseInterface
     */
    public function callback(Closure $callback): ResponseInterface;

    /**
     * @param string|array $order
     * @return Collection
     */
    public function find(string|array $order): Collection;

    /**
     * @param array $order
     * @return array|Collection
     * @throws ContainerException
     * @throws InvalidParamsException
     * @throws ServiceNotFoundException
     */
    public function refund(array $order): array|Collection;

    /**
     * @param string|array $order
     * @return array|Collection
     */
    public function close(string|array $order): array|Collection;

    /**
     * @param string|array $order
     * @return array|Collection
     * @throws ContainerException
     * @throws InvalidParamsException
     * @throws ServiceNotFoundException
     */
    public function cancel(string|array $order): array|Collection;

    /**
     * @param UserAccount $userAccount
     * @param string $outTradeNo
     * @param float $amount
     * @param string $title
     * @param string $remark
     * @return bool
     */
    public function transfer(UserAccount $userAccount, string $outTradeNo, float $amount, string $title, string $remark): bool;

    /**
     * @return bool
     */
    public function isConfig(): bool;
}
