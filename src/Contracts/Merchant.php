<?php

namespace GuanChanghu\Library\Contracts;


/**
 * @author 管昌虎
 * Interface Merchant
 * @package GuanChanghu\Library\Contracts
 * Created on 2023/5/25 23:08
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
interface Merchant
{
    /**
     * @param string $key
     * @param mixed|null $value
     * @return mixed
     */
    public function get(string $key, mixed $value = null): mixed;
}