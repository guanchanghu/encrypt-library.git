<?php
/**
 * 申请
 * Created on 2022/8/16 15:40
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts;

use Illuminate\Support\Collection;

/**
 * Class Exchange
 * @package GuanChanghu\Library\Contracts
 * Created on 2022/8/16 15:40
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Exchange
{
    /**
     * @param array $form
     * @param array $list
     * @param string $module
     * @param int $interface
     * @param int $version
     * @return Exchange
     */
    public function pushItem(string $module, int $interface, array $form = [], array $list = [], int $version = 1): Exchange;

    /**
     * @return Collection
     */
    public function clearItems(): Collection;

    /**
     * @return Collection
     */
    public function getItems(): Collection;

    /**
     * @return Collection
     */
    public function getHeaders(): Collection;


    /**
     * @return Collection
     */
    public function clearHeaders(): Collection;

    /**
     * @param string $key
     * @param string $value
     * @return Exchange
     */
    public function putHeader(string $key, string $value): Exchange;

    /**
     * @param array $headers
     * @return Exchange
     */
    public function setHeaders(array $headers): Exchange;

    /**
     * @param string $key
     * @param string $mode
     * @return Exchange
     */
    public function putLog(string $key, string $mode): Exchange;

    /**
     * @return Collection
     */
    public function getLogs(): Collection;

    /**
     * @return Collection
     */
    public function clearLogs(): Collection;

    /**
     * @param bool $isNeedToken
     * @return Exchange
     */
    public function setIsNeedToken(bool $isNeedToken): Exchange;

    /**
     * @return bool
     */
    public function getIsNeedToken(): bool;

    /**
     * @return bool
     */
    public function clearIsNeedToken(): bool;

    /**
     * @return Collection
     */
    public function request(): Collection;

    /**
     * @param string $module
     * @param int $interface
     * @return array|string
     */
    public function getResult(string $module, int $interface): array|string;
}
