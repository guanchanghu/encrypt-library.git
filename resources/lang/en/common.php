<?php return array (
  'gender' => 
  array (
    '男' => 'male',
    '女' => 'female',
    '未知' => 'unknown',
  ),
  'create' => 
  array (
    'success' => 'Successfully added',
    'error' => 'Add failed',
  ),
  'save' => 
  array (
    'success' => 'Successfully saved',
    'error' => 'Save failed',
  ),
  'change' => 
  array (
    'success' => 'Successfully modified',
    'error' => 'Modification failed',
  ),
  'remove' => 
  array (
    'success' => 'Successfully removed',
    'error' => 'Removal failed',
  ),
  'operate' => 
  array (
    'success' => 'Operation successful',
    'error' => 'operation failed',
  ),
  'export' => 
  array (
    'success' => 'Export successful',
    'error' => 'Export failed',
  ),
  'territory' => 
  array (
    'dataNull' => 'No regional data found',
  ),
  'error' => 
  array (
    'developer' => 'System error, please contact the administrator',
  ),
  'lead' => 
  array (
    'success' => 'Import successful',
    'error' => 'Import failed',
  ),
  'submit' => 
  array (
    'success' => 'Successfully submitted',
    'error' => 'Submission failed',
  ),
  'rollback' => 
  array (
    'success' => 'Recall successful',
    'error' => 'Recall failed',
  ),
  'pass' => 
  array (
    'success' => 'Successfully passed',
    'error' => 'Pass failed',
  ),
  'refuse' => 
  array (
    'success' => 'Rejected successfully',
    'error' => 'Reject failed',
  ),
  'not_belong' => 'He doesn\'t belong to you',
  '录入中' => 'Entering',
  '提交审核' => 'Submit for review',
  '审核通过' => 'Approved',
  '审核不通过' => 'Review failed',
  '暂无此状态' => 'There is currently no such status',
  'wallet' => 
  array (
    'unknown' => 'No such Models',
    'minus' => 'Models cannot be negative',
    'not_exists' => 'User wallet does not exist',
    '余额' => 'balance',
    '大币' => 'Big money',
    '小币' => 'Small coin',
  ),
  'update' => 
  array (
    'success' => 'Update successful',
    'error' => 'Update failed',
  ),
  'reset' => 
  array (
    'success' => 'Reset successful',
    'error' => 'Reset failed',
  ),
  'init' => 
  array (
    'success' => 'Initialized successfully',
    'error' => 'initialization failed',
  ),
  'delete' => 
  array (
    'success' => 'Successfully deleted',
    'error' => 'Delete failed',
  ),
  'set' => 
  array (
    'success' => 'Successfully set',
    'error' => 'Setting failed',
  ),
  'bind' => 
  array (
    'success' => 'Binding successful',
    'error' => 'Binding failed',
  ),
  'send' => 
  array (
    'success' => 'Successfully sent',
    'error' => 'fail in send',
  ),
  'config' => 
  array (
    'success' => 'Successfully configured',
    'error' => 'Configuration failed',
  ),
  'record' => 
  array (
    'success' => 'Record successful',
    'error' => 'Record failed',
  ),
  'feedback' => 
  array (
    'success' => 'Feedback successful',
    'error' => 'Feedback failed',
  ),
  'reply' => 
  array (
    'success' => 'Reply successful',
    'error' => 'Reply failed',
  ),
  'apply' => 
  array (
    'success' => 'Successfully applied',
    'error' => 'Application failed',
  ),
  'pay' => 
  array (
    'success' => 'Payment successful',
    'error' => 'Payment failed',
    'order' => 
    array (
      'none' => 'We haven\'t found this order yet',
      'repetition_paid' => 'Paid, cannot repeat payment',
      'amount_error' => 'Payment amount error',
    ),
  ),
  'close' => 
  array (
    'success' => 'Successfully closed',
    'error' => 'Close failed',
  ),
  'move' => 
  array (
    'success' => 'Successfully moved',
    'error' => 'Move failed',
  ),
  'time' => 
  array (
    'status' => 
    array (
      '未开始' => 'Not Started',
      '已开始' => 'Started',
      '已结束' => 'Ended',
      '时间范围状态不对' => 'Wrong time range status',
    ),
    'expire' => 'Ended',
  ),
  'like' => 
  array (
    'add' => 
    array (
      'success' => 'Like successful',
      'error' => 'Like failed',
    ),
    'cancel' => 
    array (
      'success' => 'Cancel likes successfully',
      'error' => 'Cancel likes failed',
    ),
  ),
  'trample' => 
  array (
    'add' => 
    array (
      'success' => 'Step on successfully',
      'error' => 'Step failure',
    ),
    'cancel' => 
    array (
      'success' => 'Cancel step successfully',
      'error' => 'Cancel step failed',
    ),
  ),
  'collect' => 
  array (
    'add' => 
    array (
      'success' => 'Collection successful',
      'error' => 'Collection failed',
    ),
    'cancel' => 
    array (
      'success' => 'Cancel Collection Successfully',
      'error' => 'Failed to cancel collection',
    ),
  ),
  'share' => 
  array (
    'success' => 'Successfully shared',
    'error' => 'Sharing failed',
  ),
  'comment' => 
  array (
    'success' => 'Comment successful',
    'error' => 'Comment failed',
  ),
  'cancel' => 
  array (
    'success' => 'Canceled successfully',
    'error' => 'Cancel failed',
  ),
  'upgrade' => 
  array (
    'success' => 'Upgrade successful',
    'error' => 'Upgrade failed',
  ),
  'format' => 
  array (
    'item_max_length' => 'After formatting, the number of digits for each large version, middle version, and small version cannot exceed :count digits',
  ),
  'lack_param' => 'Missing parameter: param',
  'sms' => 
  array (
    'hz_limit' => 'Sending SMS too frequently',
  ),
);