<?php

namespace GuanChanghu\Library\Models;


/**
 * @author 管昌虎
 * Class Territory
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/2/28 8:44
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Territory extends \GuanChanghu\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'territory';

    /**
     * 数值精度处理
     *
     * @var string[]
     */
    protected $casts = [
        'name' => 'string',
        'short_name' => 'string',
        'merger_name' => 'string',
        'floor' => 'integer',
        'postal_code' => 'integer',
        'pinyin' => 'string',
        'alpha' => 'string',
        'path' => 'string',
        'area_code' => 'string',
        'parent_id' => 'integer',
        'is_hot' => 'integer',
        'lng' => 'float',
        'lat' => 'float',
    ];
}
