<?php
/**
 * 身份验证器
 * Created on 2022/4/15 17:18
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\Authenticator as AuthenticatorContract;
use Illuminate\Support\Facades\Facade;

/**
 * @author 管昌虎
 * Class Authenticator
 * @method static string createSecret()
 * @method static string scan(string $secret = '', string $label = '')
 * @method static bool verify(string $secret, string $code)
 * @package GuanChanghu\Library\Facades
 * Created on 2022/4/15 17:18
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Authenticator extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return AuthenticatorContract::class;
    }
}
