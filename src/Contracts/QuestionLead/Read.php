<?php


namespace GuanChanghu\Library\Contracts\QuestionLead;

use Illuminate\Support\Collection;
use Generator;

/**
 * Interface Lead
 * @package GuanChanghu\Library\Contracts\Lead\Question
 */
interface Read
{
    /**
     * Lead constructor.
     */
    public function __construct();

    /**
     * @return void
     */
    public function checkAll(): void;

    /**
     * @return Collection
     */
    public function read(): Collection|Generator;

    /**
     * @param string $type
     * @return void
     */
    public function checkTypeSegmentation(string $type): void;

    /**
     * @return string
     */
    public function getFile(): string;

    /**
     * @param string $file
     * @return Read
     */
    public function file(string $file): Read;

    /**
     * @return void
     */
    public function checkAllTypeSegmentation(): void;

    /**
     * @return void
     */
    public function checkLine(): void;

    /**
     * @return void
     */
    public function checkStartString(): void;

    /**
     * @return void
     */
    public function checkTopicNumber(): void;

    /**
     * @return void
     */
    public function checkSubject(): void;

    /**
     * @return void
     */
    public function checkDifficulty(): void;
}
