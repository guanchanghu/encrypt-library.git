<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailSendLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_send_logs', function (Blueprint $table) {
            $table->id();

            $table->string('from_email')->default('')->comment('发送邮件的邮箱');

            $table->string('email', 40)->default('')->comment('邮箱');

            $table->string('title', 100)->default('')->comment('标题');

            $table->char('captcha', 6)->default('')->comment('如果是验证码');

            $table->timestamps();

            $table->index(['from_email', 'email']);

            $table->comment('邮件发送日志表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_send_logs');
    }
}
