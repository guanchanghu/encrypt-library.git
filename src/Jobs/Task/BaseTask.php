<?php


namespace GuanChanghu\Library\Jobs\Task;

use GuanChanghu\Library\Facades\Client;
use Hhxsv5\LaravelS\Swoole\Task\Task;
use Illuminate\Support\Facades\Log;
use Throwable;

/**
 * @author 管昌虎
 * Class BaseTask
 * @tag encryption free
 * @package GuanChanghu\Library\Jobs\Task
 * Created on 2022/2/18 8:30
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
abstract class BaseTask extends Task
{
    use \GuanChanghu\Traits\Log\Facade;
    use \GuanChanghu\Traits\SafeOperation\Facade;

    /**
     * @var string
     */
    protected string $uuid = '';

    /**
     * Encrypt constructor.
     * @param string $client
     */
    public function __construct(protected string $client = '')
    {
        $this->client = Client::client($client)->getClient();
        $this->uuid = Client::client($client)->getUuid();
    }

    /**
     * @param Throwable $throwable
     * @return void
     */
    public function throwable(Throwable $throwable): void
    {
        $context = [$throwable->getFile() . ':' . $throwable->getLine(), $throwable->getMessage()];

        foreach ($this->logContext() as $filed => $value) {
            $context[$filed] = $value;
        }

        Log::channel($this->getLogChannel($this->client))->error('uuid：' . $this->uuid, $context);
    }

    /**
     * @param array $parameters
     */
    protected function setParameter(array $parameters): void
    {
        foreach ($parameters as $field => $value) {
            $this->$field = $value;
        }
    }

    /**
     * @return array
     */
    public function logContext(): array
    {
        return [];
    }
}
