<?php


namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\QuestionLead\Factory;
use Illuminate\Support\Facades\Facade;
use GuanChanghu\Library\Contracts\QuestionLead\Core;
use GuanChanghu\Library\Contracts\QuestionLead\Read;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Question
 * @method static Core driver(string $driver)
 * @method static Read file(string $file)
 * @method static string getFile()
 * @method static checkAll()                                                                                            必须先调用file
 * @method static lead()                                                                                                必须先调用file
 * @method static lines(Collection $collection, int $parentId = 0)
 * @method static Model|null line(Collection $collection, int $parentId = 0)
 * @method static txtToJson(string $toFile = '')                                                                        必须先调用file,只支持txt驱动
 * @method static checkTypeSegmentation(string $type)                                                                   必须先调用file,只支持txt驱动
 * @method static checkAllTypeSegmentation()                                                                            必须先调用file,只支持txt驱动
 * @method static checkLine()                                                                                           必须先调用file,只支持txt驱动
 * @method static checkStartString()                                                                                    必须先调用file,只支持txt驱动
 * @method static checkTopicNumber()                                                                                    必须先调用file,只支持txt驱动
 * @method static checkSubject()                                                                                        必须先调用file,只支持txt驱动
 * @method static checkDifficulty()                                                                                     必须先调用file,只支持txt驱动
 * @package GuanChanghu\Library\Facades\Lead
 */
class QuestionLead extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
