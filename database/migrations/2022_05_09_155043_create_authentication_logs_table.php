<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthenticationLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authentication_logs', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->default(0)->comment('用户ID');

            $table->unsignedTinyInteger('certificate_type')->default(0)->comment('证件类型;0-身份证;1-驾驶证;2-护照');
            $table->string('driver')->default('artificial')->comment('认证驱动;artificial-人工验证;ali-阿里云实人认证;juhe-聚合认证;tencent-腾讯;...');

            $table->string('pay_driver', 32)->default('wallet')->comment('支付驱动;wallet-钱包支付;ali-支付宝;weChat-微信;...');

            $table->decimal('money', 12, 2)->default(0)->comment('支付金额');

            $table->unsignedBigInteger('user_wallet_log_id')->default(0)->comment('如果是钱包支付,钱包日志ID');

            $table->unsignedBigInteger('award_user_wallet_log_id')->default(0)->comment('如果是认证有奖励,钱包日志ID');

            $table->char('order_no', 32)->nullable()->comment('订单号');

            $table->string('real_name', 25)->default('')->comment('真实姓名');
            $table->char('id_number', 18)->default('')->comment('身份证号码');

            $table->string('front_image')->default('')->comment('证件前面图片');
            $table->string('back_image')->default('')->comment('证件后面图片');
            $table->string('hand_image')->default('')->comment('证件手持图片');

            $table->unsignedTinyInteger('status')->default(0)->comment('状态;0-申请中/认证中;1-已认证/认证成功;2-认证拒绝/认证失败');

            $table->timestamp('passed_at')->nullable()->comment('通过时间');
            $table->timestamp('refused_at')->nullable()->comment('拒绝时间');

            $table->string('remark')->default('')->comment('拒绝备注');

            $table->timestamps();

            $table->index(['user_id', 'status']);

            $table->index(['id_number', 'status']);

            $table->unique(['order_no']);

            $table->index(['user_wallet_log_id']);
            $table->index(['award_user_wallet_log_id']);

            $table->comment('用户实人认证日志表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authentication_logs');
    }
}
