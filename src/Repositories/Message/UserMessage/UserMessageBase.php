<?php

namespace GuanChanghu\Library\Repositories\Message\UserMessage;


use GuanChanghu\Library\Contracts\Repositories\AutoQuery;
use GuanChanghu\Library\Contracts\Repositories\Order;
use GuanChanghu\Library\Contracts\Repositories\Query;
use GuanChanghu\Library\Facades\ModelQuery;
use GuanChanghu\Library\Repositories\Base\Facade;

/**
 * @author 管昌虎
 * Class UserMessageBase
 * @tag encryption free
 * @package GuanChanghu\Library\Repositories\Message\UserMessage
 * Created on 2023/4/2 12:06
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class UserMessageBase extends Facade implements Order, AutoQuery
{
    use \GuanChanghu\Library\Repositories\Base\Traits\Order;
    use \GuanChanghu\Library\Repositories\Base\Traits\AutoQuery;

    /**
     * @return Query
     */
    public function initModel(): Query
    {
        $modelConfig = config('guan-changhu.models.user_message');

        $this->model = new $modelConfig();

        return $this;
    }

    /**
     * @return Query
     */
    public function query(): Query
    {
        ModelQuery::queryWithUser($this->builder());

        return parent::query();
    }
}
