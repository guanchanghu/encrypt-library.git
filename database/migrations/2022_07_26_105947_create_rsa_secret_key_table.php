<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRsaSecretKeyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rsa_secret_key', function (Blueprint $table) {
            $table->id();

            $table->string('client')->default('')->comment('客户端');

            $table->unsignedInteger('bits')->default(2048)->comment('bits');

            $table->text('public_key')->nullable()->comment('公钥');

            $table->text('private_key')->nullable()->comment('私钥');

            $table->unsignedTinyInteger('encryption_mode')->default(1)->comment('加密模式');

            $table->unsignedTinyInteger('signature_mode')->default(1)->comment('加签模式');

            $table->text('comment')->nullable()->comment('注释');

            $table->timestamps();

            $table->unique(['client']);

            $table->comment('rsa秘钥表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rsa_secret_key');
    }
}
