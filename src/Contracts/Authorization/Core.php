<?php


namespace GuanChanghu\Library\Contracts\Authorization;

/**
 * Interface Facade
 * @package GuanChanghu\Library\Contracts\AuthorizationConfig
 */
interface Core
{
    /**
     * @return void
     */
    public function validate(): void;
}
