<?php

namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\Upload\Core;
use GuanChanghu\Library\Contracts\Upload\Factory;
use GuanChanghu\Library\Models\FileStoreRecord;
use GuanChanghu\Library\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Facade;

/**
 * Class Upload
 * @method static Core driver(string $driver = '')
 * @method static FileStoreRecord upload(User $user, Request $request)
 * @method static FileStoreRecord copy(User $user, string $sourceFile, $client = '', $module = 'all')
 * @package GuanChanghu\Library\Facades
 */
class Upload extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
