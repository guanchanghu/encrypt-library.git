<?php

namespace GuanChanghu\Library\Models;


use Illuminate\Support\Collection;

/**
 * @author 管昌虎
 * Class SystemConfig
 * @tag encryption free
 * @package App\Models
 * Created on 2022/2/28 8:44
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class SystemConfig extends \GuanChanghu\Models\SystemConfig\Facade implements \GuanChanghu\Models\Contracts\Recycle, \GuanChanghu\Models\Contracts\TableAttribute
{
    /**
     * 客户端配置
     */
    public const CLIENT_CONFIG_KEY = 'client_config';

    /**
     * 钱包配置
     */
    public const WALLET_CONFIG_KEY = 'wallet_config';

    /**
     * @return Collection
     */
    public static function getClientConfig(): Collection
    {
        return static::get(static::CLIENT_CONFIG_KEY);
    }

    /**
     * @return Collection
     */
    public static function getWalletConfig(): Collection
    {
        return static::get(static::WALLET_CONFIG_KEY);
    }

    /**
     * @return void
     */
    public static function checkSetClientConfig(): void
    {
        // 你规定的生存时间是以分钟为单位,完整的生存时间  ： 7天  + 你规定的生存时间（单位分钟）
        $config = static::getClientConfig();

        if ($config->isEmpty()) {
            $clientExpire = [];
            $clients = config('guan-changhu.clients.list');

            foreach ($clients as $client) {
                $clientExpire[$client] = 60 * 24 * 30;
            }
            static::set(static::CLIENT_CONFIG_KEY, ['clientExpire' => $clientExpire, 'defaultForbidPermission' => []]);
        }
    }
}
