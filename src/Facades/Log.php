<?php

namespace GuanChanghu\Library\Facades;


use GuanChanghu\Library\Contracts\Log\Factory;
use Illuminate\Support\Facades\Facade;

/**
 * @author 管昌虎
 * Class Log
 * @method static Factory channel(string $channel = null)
 * @method static string clearChannel()
 * @method static Factory setClient(string $client)
 * @method static string clearClient()
 * @method static Factory onQueue(string $queue)
 * @method static string clearQueue()
 * @method static Factory setLogType(int $logType)
 * @method static string clearLogType()
 * @method static Factory setSlowLog(bool $isSlowLog)
 * @method static string clearSlowLog()
 * @method static Factory setDelay(int $delay)
 * @method static int clearDelay()
 * @method static void alert(string $message, array $context = [])
 * @method static void critical(string $message, array $context = [])
 * @method static void debug(string $message, array $context = [])
 * @method static void emergency(string $message, array $context = [])
 * @method static void error(string $message, array $context = [])
 * @method static void info(string $message, array $context = [])
 * @method static void log(string $message, array $context = [])
 * @method static void notice(string $message, array $context = [])
 * @method static void warning(string $message, array $context = [])
 * @method static void write(string $level, string $message, array $context = [])
 * @package GuanChanghu\Library\Facades
 * Created on 2023/4/5 11:12
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Log extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
