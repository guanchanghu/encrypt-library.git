<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmsSendLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_send_logs', function (Blueprint $table) {
            $table->id();

            $table->string('driver', 50)->default('')->comment('驱动/网关');

            $table->string('mobile', 14)->default('')->comment('手机号');

            $table->string('key',35)->default('')->comment('key');

            $table->string('template')->default('')->comment('模板');
            $table->string('content')->default('')->comment('文字内容');

            $table->json('data')->nullable()->comment('参数');

            $table->timestamps();

            $table->index(['mobile', 'key']);

            $table->comment('sms发送日志表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_send_logs');
    }
}
