<?php
/**
 * 授权门面
 * Created on 2022/4/6 13:18
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\Auth\Factory;
use GuanChanghu\Library\Contracts\Auth\Core;
use GuanChanghu\Library\Models\User;
use Illuminate\Support\Facades\Facade;

/**
 * @author 管昌虎
 * Class Auth
 * @package GuanChanghu\Library\Facades
 * @method static Core way(string $way = '')
 * @method static array createBearerToken(User $user, string $client, int $seconds)
 * @method static bool isOnline(int $userId, string $client)
 * @method static void logout(User $user, string $client)
 * @method static User|null resolve(string $client)
 * @method static void expire()
 * Created on 2022/4/6 13:18
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Auth extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
