<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTerritoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('territory', function (Blueprint $table) {
            $table->id();

            $table->string('name', 50)->default('')->index()->comment('行政单位名称');
            $table->string('short_name', 50)->default('')->index()->comment('行政单位简称');
            $table->string('merger_name', 50)->default('')->index()->comment('组合名');
            $table->unsignedInteger('floor')->default(1)->comment('行政单位层级');
            $table->string('pinyin')->default('')->index()->comment('行政单位拼音');
            $table->char('alpha', 1)->index()->default('')->comment('行政单位拼音开头字符');
            $table->unsignedBigInteger('parent_id')->index()->default(0)->comment('行政单位上级');
            $table->string('path')->default('')->index()->comment('行政单位上级路径');
            $table->char('area_code', 6)->default('')->index()->comment('区号');
            $table->unsignedMediumInteger('postal_code')->default('000000')->index()->comment('邮政编码');
            $table->decimal('lng', 10, 6)->default(0)->comment('所处经度');
            $table->decimal('lat', 10, 6)->default(0)->comment('所处纬度');
            $table->unsignedTinyInteger('is_hot')->default(0)->comment('是否热门；0-不是热门；1-是热门');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('territory');
    }
}
