<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatisticsRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistics_records', function (Blueprint $table) {
            $table->id();

            $table->string('table')->default('')->comment('table type');

            $table->string('type')->default('')->comment('统计类型');

            $table->string('link')->default('')->comment('链接');

            $table->unsignedSmallInteger('year')->default(0)->comment('年;如2022');
            $table->char('month', 7)->default('')->comment('月;如 2022-02');
            $table->char('day', 10)->default('')->comment('日;如 2022-02-21');
            $table->char('hour', 13)->default('')->comment('小时;如 2022-02-21 16');

            $table->decimal('value', 12, 2)->default(0)->comment('值');

            $table->index(['table', 'type', 'hour', 'value'], 'sr_t_t_h_v');

            $table->timestamps();

            $table->comment('统计记录表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistics_records');
    }
}
