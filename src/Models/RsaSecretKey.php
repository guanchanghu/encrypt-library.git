<?php
/**
 * rsa秘钥
 * Created on 2022/7/26 11:49
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Models;

use GuanChanghu\Exceptions\DeveloperException;
use GuanChanghu\Traits\Cache\Facade as CacheTrait;

/**
 * @author 管昌虎
 * Class RsaSecretKey
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/7/26 11:50
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class RsaSecretKey extends \GuanChanghu\Models\Model
{
    use CacheTrait;

    /**
     * @var string
     */
    protected $table = 'rsa_secret_key';

    /**
     * @var array
     */
    protected $casts = [
        'bits' => 'integer',
        'client' => 'string',
        'public_key' => 'string',
        'private_key' => 'string',
        'encryption_mode' => 'integer',
        'signature_mode' => 'integer',
    ];


    /**
     * @param string $client
     * @return static
     * @throws DeveloperException
     */
    public static function rsaSecretKey(string $client): static
    {
        return static::tags()->rememberForever($client, function () use ($client) {
            $config = static::query()->where('client', $client)->first();

            if (!$config) {
                throw new DeveloperException('无此客户端');
            }

            return $config;
        });
    }

    /**
     * @param string $client
     * @return bool
     */
    public static function rsaSecretKeyForget(string $client): bool
    {
        return static::tags()->forget($client);
    }
}
