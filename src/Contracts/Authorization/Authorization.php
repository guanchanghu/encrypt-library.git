<?php


namespace GuanChanghu\Library\Contracts\Authorization;


/**
 * Interface AuthorizationConfig
 * @package GuanChanghu\Library\Contracts\AuthorizationConfig
 */
interface Authorization
{
    /**
     * @return void
     */
    public function validate(): void;
}
