<?php
/**
 * 邮件发送日志表
 * Created on 2022/4/25 14:55
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Models;


/**
 * @author 管昌虎
 * Class EmailSendLogs
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/4/25 14:56
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class EmailSendLog extends \GuanChanghu\Models\Model
{
    /**
     * 数值精度处理
     *
     * @var string[]
     */
    protected $casts = [
        'from_email' => 'string',
        'title' => 'string',
        'email' => 'string',
        'captcha' => 'string',
    ];
}
