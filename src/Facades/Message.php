<?php
/**
 * 消息门面
 * Created on 2022/3/31 16:48
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\Message\Factory;
use GuanChanghu\Library\Contracts\Message\Core;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Collection;

/**
 * @author 管昌虎
 * Class Message
 * @method static Core driver(string $driver = '')
 * @method static Core set(array $params)                                                                               设置参数
 * @method static array clear()                                                                                         清空并且返回清空的数据
 * @method static Model send(int $userId, string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0)                                              给全部客户端发送信息
 * @method static Model sendByClient(string $client, int $userId, string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0)                      给指定客户端发送信息
 * @method static Model sendExcludeClients(array $excludeClients, int $userId, string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0)         排除指定客户端发送信息
 * @method static Model sendClientMany(array $clients, int $userId, string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0)                    给多个客户端发送信息
 * @method static Model sendMany(array $clients, Collection|array $userIds, string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0)                       给多个客户端多个用户发送信息
 * @method static Model sendAll(string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0)                       给全部用户全部客户端发送信息
 * @method static Model sendAllClientMany(array $clients, string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0)                       给全部用户指定客户端发送信息
 * @method static Model supplyClientMany(string $client, int $userId)                       补发,给全部用户发送的信息
 * @method static Model getMessage(int|string $massageId, int $userId = 0)                                       获得信息
 * @method static string jump(int|string $massageId, int $userId = 0)                                                       获得跳转信息
 * @method static Model read(int|string $massageId, int $userId = 0)                                             标记为已读
 * @package GuanChanghu\Library\Facades
 * Created on 2022/3/31 16:48
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Message extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
