<?php


namespace GuanChanghu\Library\Contracts\Territory;


use Illuminate\Contracts\Foundation\Application;

/**
 * Interface Factory
 * @package GuanChanghu\Library\Contracts\Territory
 */
interface Factory
{
    /**
     * Factory constructor.
     * @param Application $app
     */
    public function __construct(Application $app);

    /**
     * @param string $store
     * @return Core
     */
    public function store(string $store = ''): Core;
}
