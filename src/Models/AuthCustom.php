<?php
/**
 * 自定义授权表
 * Created on 2022/4/6 14:36
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Models;

/**
 * @author 管昌虎
 * Class AuthCustom
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/4/6 14:36
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class AuthCustom extends \GuanChanghu\Models\Model
{
    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * 主键是否主动递增
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * 数值精度处理
     *
     * @var string[]
     */
    protected $casts = [
        'user_id' => 'integer',
        'client' => 'string',
        'expires_at' => 'datetime',
    ];
}
