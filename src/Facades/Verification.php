<?php
/**
 * 人机验证门面
 * Created on 2022/4/15 14:58
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use Illuminate\Support\Facades\Facade;
use GuanChanghu\Library\Contracts\Verification\Factory;
use GuanChanghu\Library\Contracts\Verification\Core;

/**
 * @author 管昌虎
 * Class Verification
 * @method static Core driver(string $driver = '')
 * @method static bool validate(array $params, string $way = '')   目前提供阿里谷歌两种人机验证,第二个参数表示方式,google 只有一种,不用传递,阿里,提供滑动验证,无痕验证,智能验证  analyzeNvc 表示无痕验证,其他两种参数一致,不用区分
 * @package GuanChanghu\Library\Facades
 * Created on 2022/4/15 14:59
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Verification extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
