<?php


namespace GuanChanghu\Library\Facades;


use GuanChanghu\Library\Contracts\Permission as PermissionContract;
use GuanChanghu\Library\Models\User;
use Illuminate\Support\Facades\Facade;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Collection;

/**
 * Class Permission
 * @method static Collection getRolePermissions(string $client)
 * @method static void updateRole()
 * @method static void updateUserPermission(User $user)
 * @method static void assignRoles(User $user, string $client, array $roles)
 * @method static void assignRoleByClient(User $user, string $client)
 * @method static string resolveRoleByPermission(string $client, string $permission)
 * @method static Role assignRoleByModule(User $user, string $client, int $version, string $module)
 * @method static Role removeRoleByModule(User $user, string $client, int $version, string $module)
 * @method static void assignRolePermission(int $version, Role $role)
 * @package GuanChanghu\Library\Facades
 */
class Permission extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return PermissionContract::class;
    }
}
