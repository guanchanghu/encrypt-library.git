<?php


namespace GuanChanghu\Library\Contracts\Territory;

use Illuminate\Support\Collection;

/**
 * Interface Store
 * @package GuanChanghu\Library\Contracts\Territory
 */
interface Store
{
    /**
     * @param int $id
     * @return array
     */
    public function getTerritoryById(int $id): array;

    /**
     * @param string $shortName
     * @return array
     */
    public function getTerritoryByShortName(string $shortName): array;

    /**
     * @param string $name
     * @return array
     */
    public function getTerritoryByName(string $name): array;

    /**
     * @return string
     */
    public function getPrimaryKey(): string;

    /**
     * @return Collection
     */
    public function getAllTerritory(): Collection;

    /**
     * @param int $parentId
     * @return Collection
     */
    public function getTerritorySubordinateByParentId(int $parentId): Collection;

    /**
     * @return bool
     */
    public function clear(): bool;
}
