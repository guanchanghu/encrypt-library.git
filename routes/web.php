<?php

use GuanChanghu\Library\Http\Controllers\DocumentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('doc/{client}/{version}', [DocumentController::class, 'apiDoc'])->where('client', implode('|', config('guan-changhu.clients.list')));
Route::get('doc/rule', [DocumentController::class, 'apiDocRule']);
Route::get('doc/upload', [DocumentController::class, 'apiDocUpload']);
