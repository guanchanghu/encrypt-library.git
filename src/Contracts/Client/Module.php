<?php


namespace GuanChanghu\Library\Contracts\Client;

use GuanChanghu\Library\Contracts\Repositories\Attribute as PaginateAttribute;
use GuanChanghu\Library\Models\User;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;

/**
 * Interface ModuleApi
 * @package GuanChanghu\Library\Contracts\Client
 */
interface Module
{
    /**
     * Encrypt constructor.
     * @param string $client
     * @param Controller|null $controller
     */
    public function __construct(string $client, Controller|null $controller);

    /**
     * 模块名
     * @return string
     */
    public function moduleName(): string;

    /**
     * @return array
     */
    public static function interfaceCode(): array;

    /**
     * 接口列表
     * @return Collection
     */
    public function interfaceList(): Collection;

    /**
     * 不需要登录验证的接口
     * @return Collection
     */
    public function anonymityList(): Collection;

    /**
     * 是否需要事务
     * @return Collection
     */
    public function transactionList(): Collection;

    /**
     * 设置用户
     * @param User|null $user
     * @return void
     */
    public function setUser(User|null $user): void;

    /**
     * 获得用户
     * @return User
     */
    public function user(): User;

    /**
     * 获得用户
     * @return User|null
     */
    public function lazyUser(): User|null;

    /**
     * 是否登录
     * @return bool
     */
    public function isLogin(): bool;

    /**
     * 获得用户ID
     * @return int
     */
    public function userId(): int;

    /**
     * @param Collection $form
     * @param PaginateAttribute $paginateAttribute
     * @return void
     */
    public function interfaceBefore(Collection $form, PaginateAttribute $paginateAttribute): void;

    /**
     * @param Collection $form
     * @param PaginateAttribute $paginateAttribute
     * @return void
     */
    public function interfaceAfter(Collection $form, PaginateAttribute $paginateAttribute): void;

    /**
     * @return string
     */
    public function client(): string;

    /**
     * 判断允不允许请求
     * @param int $interface
     * @param array $moduleSwitch
     * @return void
     */
    public function isAllow(int $interface, array $moduleSwitch): void;

    /**
     * 获得权限路径
     * @param int $interface
     * @return string
     */
    public function getPermissionPath(int $interface): string;

    /**
     * 模块开关状态
     * @param array $moduleSwitch
     * @return bool
     */
    public function switch(array $moduleSwitch): bool;

    /**
     * 获得权限label
     * @param int $interface
     * @return string
     */
    public function getPermissionLabel(int $interface): string;

    /**
     * @return string
     */
    public function getModuleNameEnglish(): string;

    /**
     * 获得接口方法名
     * @param int $interface
     * @return string
     */
    public function getModuleMethod(int $interface): string;

    /**
     * 判断接口需不需要事务
     * @param int $interface
     * @return bool
     */
    public function isTransaction(int $interface): bool;

    /**
     * 获得权限
     * @param int $interface
     * @return string
     */
    public function getModuleMethodPermission(int $interface): string;
}
