<?php
/**
 * 财务相关翻译
 * Created on 2022/3/18 16:42
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

return [
    'error' => [
        'unknown' => '无此订单',
        'pay_driver' => '订单渠道不对',
        'amount' => '订单数值不对',
    ],
    'recharge' => [
        'error' => [
            'min' => '充值金额不能小于 :min',
            'max' => '充值金额不能大于 :max',
            'multiple' => '充值金额必须是 :multiple 的整数倍',
            'repetition' => '订单不能重复使用',
        ],
        'success' => '充值成功',
    ],
    'transfer' => [
        'success' => '转账成功',
        'error' => '转账失败',
        'receipt' => [
            'success' => '接受成功',
            'error' => '接受失败',
        ],
        'refuse' => [
            'success' => '拒绝成功',
            'error' => '拒绝失败',
        ],
    ],
    'convert' => [
        'success' => '兑换成功',
        'error' => '兑换失败',
        'wallet_eq' => '转换钱包不能选择一样的',
        'switch' => ':fromWallet 不能向 :wallet 兑换',
    ],
    'withdraw' => [
        'channel_error' => '暂不提供此驱动方式提现',
        'auto_error' => '自动提现失败',
    ],
    'wallet' => [
        'unknown' => '暂无此钱包',
        'minus' => ':walletName 不足',
        'not_exists' => '用户钱包不存在',
        '余额' => '余额',
        '大币' => '大币',
        '小币' => '小币',
        '积分' => '积分',
        'not_open' => '此钱包尚未开启',
    ],
    'pay_way_error' => '支付方式不对',
    '账户微信' => '账户微信',
    'award' => [
        'register_close' => '注册奖励已关闭'
    ],
    'pay' => [
        'driver_close' => '支付方式已关闭，请更换一种试一试',
        'wallet_close' => '该种钱包支付方式已关闭，请更换一种试一试',
        'success' => '支付成功',
        'error' => '支付失败',
    ],
    'red_package' => [
        'receive' => '红包已被领取完毕',
        'expire' => '红包已过期',
        'error' => '领取红包失败',
        'draw_exists' => '红包不能重复领取',
        'receiver_error' => '这个红包不是发送给你的，你不能领取',
        'receiver_empty_error' => '请指定接收人',
        'friend_group_empty_error' => '请指定发送的群组',
        'status_error' => '红包状态错误',
        'draw_friend' => '只能是好友才能领取此红包',
        'friend_group_error' => '好友群组错误',
        'friend_group_send_error' => '你不在此群中，不能发送红包',
        'close' => '发红包功能已关闭',
        'wallet_close' => '此钱包发红包功能已关闭',
        'min_error' => '发的红包太小了，必须大于 :min',
        'max_error' => '发的红包太大了，必须小于 :max',
        'max_num_error' => '发的红包数量过多，必须小于 :max',
        'money_too_small' => '您发的红包总金额太小了，不够人手一个的',
    ],
];
