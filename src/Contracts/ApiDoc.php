<?php


namespace GuanChanghu\Library\Contracts;

use GuanChanghu\Library\Contracts\Client\Module;

/**
 * Interface ApiDoc
 * @package GuanChanghu\Library\Contracts
 */
interface ApiDoc
{
    /**
     * @param Module $class
     * @return array
     */
    public function parserClass(Module $class): array;

    /**
     * @param Module $class
     * @param string $method
     * @return array
     */
    public function parserMethod(Module $class, string $method): array;

    /**
     * @param string $client
     * @param int $version
     * @return bool
     */
    public function generateApiDoc(string $client = '', int $version = 0): bool;

    /**
     * @param string $doc
     * @return array
     */
    public function parse(string $doc): array;
}
